﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using NZWalks.API.CustomActionFilters;
using NZWalks.API.Data;
using NZWalks.API.Mappings;
using NZWalks.API.Models.Domain;
using NZWalks.API.Models.DTO;
using NZWalks.API.Repositories;

namespace NZWalks.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WalksController : ControllerBase
    {
        private readonly NZWalksDbContext dbContext;
        private readonly IMapper mapper;
        private readonly IWalkRepository walkRepository;

        public WalksController(NZWalksDbContext dbContext, IMapper mapper,IWalkRepository walkRepository)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.walkRepository = walkRepository;
        }
        //create Walk
        //Post /api/walks



        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] string? filterOn, [FromQuery] string? filterQuery ,
            [FromQuery] string? sortBy, [FromQuery] bool? isAscending,
            [FromQuery] int pageNumber = 1, [FromQuery] int pageSize =1000)
        {
           var domainWalks = await walkRepository.GetAllAsync(filterOn,filterQuery, sortBy,isAscending ?? true,pageNumber,pageSize);

            
            return Ok(mapper.Map<List<WalkDto>>(domainWalks));
        }
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById([FromRoute]Guid id)
        {
            var walkDomain = await walkRepository.GetById(id);

            if(walkDomain == null)
            {
                return NotFound();
            }

            return Ok(mapper.Map<WalkDto>(walkDomain));
        }
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Create([FromBody]AddWalkRequestDto addWalkRequestDto)
        {
          
                var walkDomain = mapper.Map<Walk>(addWalkRequestDto);
                walkDomain = await walkRepository.CreatAsync(walkDomain);



                return Ok(mapper.Map<AddWalkRequestDto>(walkDomain));
            
          
        }
        [HttpPut]
        [Route("{id:Guid}")]
        [ValidateModel]
        public async Task<IActionResult>Update([FromRoute] Guid id, [FromBody] UpdateWalksDto updateWalksDto)
        {
           
                var walkDomain = mapper.Map<Walk>(updateWalksDto);

                if (walkDomain == null)
                {
                    return NotFound();
                }
                await walkRepository.Update(id, walkDomain);

                var walkDto = mapper.Map<WalkDto>(walkDomain);

                return Ok(walkDto);
           


        }
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var domain= await walkRepository.DeleteById(id);

            if (domain == null)
            {
                return NotFound();
            }

            var dtoDelete = mapper.Map<WalkDto>(domain);

            return Ok(dtoDelete);
        }
    }
}
