﻿using Microsoft.EntityFrameworkCore;
using NZWalks.API.Models.Domain;

namespace NZWalks.API.Data
{
    public class NZWalksDbContext : DbContext
    {
        public NZWalksDbContext(DbContextOptions <NZWalksDbContext>dbContextOptions) :base(dbContextOptions)
        {
     
        }
        public DbSet<Difficulty> Difficulties { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Walk> Walks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var difficulties = new List<Difficulty>() {
                new Difficulty()
                {
                    Id=Guid.Parse("d812bc0b-a79c-42e6-8a02-00fb46e4e131"),
                    Name="Easy"
                },
                new Difficulty()
                {
                    Id=Guid.Parse("3516dfb3-4d24-4360-83bd-847241d02c62"),
                    Name="Medium"
                },
                new Difficulty()
                {
                    Id=Guid.Parse("369411f7-56e6-4bfc-94b2-4e0f7187823d"),
                    Name="Hard"
                },
            };
            modelBuilder.Entity<Difficulty>().HasData(difficulties);


            //seed data for regions

            var regions = new List<Region>()
            {
                new Region()
                {
                    Id=Guid.Parse("fed82d90-3092-4af8-8c01-c89ccf17f0de"),
                    Name="Auckland",
                    Code = "AKL",
                    RegionImageUrl="https://images.pexels.com/photos/831910/pexels-photo-831910.jpeg?auto=compress&cs=tinysrgb&w=600"
                },
                new Region()
                {
                    Id=Guid.Parse("5fdae13d-5caa-46ad-acd9-fe25f82ce1d9"),
                    Name="Northland",
                    Code = "NTL",
                    RegionImageUrl=null
                },
                new Region()
                {
                    Id=Guid.Parse("2cd910e6-6852-4571-b21f-91126a672f6d"),
                    Name="Bay Of Plenty",
                    Code = "Bop",
                    RegionImageUrl=null
                },
                 new Region()
                {
                    Id=Guid.Parse("81d51d8d-c6dd-46c4-875d-6b5fcc0aea08"),
                    Name="Wellington",
                    Code = "WGN",
                    RegionImageUrl="https://images.pexels.com/photos/8379417/pexels-photo-8379417.jpeg?auto=compress&cs=tinysrgb&w=600"
                },
                  new Region()
                {
                    Id=Guid.Parse("31f3d663-ecd2-41d4-8c6c-319ea518feca"),
                    Name="Nelson",
                    Code = "NSN",
                    RegionImageUrl="https://images.pexels.com/photos/14905684/pexels-photo-14905684.jpeg?auto=compress&cs=tinysrgb&w=600"
                },
                  new Region()
                  {
                    Id=Guid.Parse("c4cff671-6b78-44e6-956e-4a0e19fcf9ec"),
                    Name="Southland",
                    Code = "STL",
                    RegionImageUrl=null
                },
            };
            modelBuilder.Entity<Region>().HasData(regions);
        }
    }
}
