﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace NZWalks.API.Migrations
{
    /// <inheritdoc />
    public partial class Seedingdatafordiffictultiesandregions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Difficulties",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("3516dfb3-4d24-4360-83bd-847241d02c62"), "Medium" },
                    { new Guid("369411f7-56e6-4bfc-94b2-4e0f7187823d"), "Hard" },
                    { new Guid("d812bc0b-a79c-42e6-8a02-00fb46e4e131"), "Easy" }
                });

            migrationBuilder.InsertData(
                table: "Regions",
                columns: new[] { "Id", "Code", "Name", "RegionImageUrl" },
                values: new object[,]
                {
                    { new Guid("2cd910e6-6852-4571-b21f-91126a672f6d"), "Bop", "Bay Of Plenty", null },
                    { new Guid("31f3d663-ecd2-41d4-8c6c-319ea518feca"), "NSN", "Nelson", "https://images.pexels.com/photos/14905684/pexels-photo-14905684.jpeg?auto=compress&cs=tinysrgb&w=600" },
                    { new Guid("5fdae13d-5caa-46ad-acd9-fe25f82ce1d9"), "NTL", "Northland", null },
                    { new Guid("81d51d8d-c6dd-46c4-875d-6b5fcc0aea08"), "WGN", "Wellington", "https://images.pexels.com/photos/8379417/pexels-photo-8379417.jpeg?auto=compress&cs=tinysrgb&w=600" },
                    { new Guid("c4cff671-6b78-44e6-956e-4a0e19fcf9ec"), "STL", "Southland", null },
                    { new Guid("fed82d90-3092-4af8-8c01-c89ccf17f0de"), "AKL", "Auckland", "https://images.pexels.com/photos/831910/pexels-photo-831910.jpeg?auto=compress&cs=tinysrgb&w=600" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Difficulties",
                keyColumn: "Id",
                keyValue: new Guid("3516dfb3-4d24-4360-83bd-847241d02c62"));

            migrationBuilder.DeleteData(
                table: "Difficulties",
                keyColumn: "Id",
                keyValue: new Guid("369411f7-56e6-4bfc-94b2-4e0f7187823d"));

            migrationBuilder.DeleteData(
                table: "Difficulties",
                keyColumn: "Id",
                keyValue: new Guid("d812bc0b-a79c-42e6-8a02-00fb46e4e131"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("2cd910e6-6852-4571-b21f-91126a672f6d"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("31f3d663-ecd2-41d4-8c6c-319ea518feca"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("5fdae13d-5caa-46ad-acd9-fe25f82ce1d9"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("81d51d8d-c6dd-46c4-875d-6b5fcc0aea08"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("c4cff671-6b78-44e6-956e-4a0e19fcf9ec"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("fed82d90-3092-4af8-8c01-c89ccf17f0de"));
        }
    }
}
